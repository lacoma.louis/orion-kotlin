package iutinfo.lp.orion.kotlin

import android.content.Intent
import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import iutinfo.lp.orion.kotlin.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)
    var activityRule2: ActivityTestRule<ConnectActivity> = ActivityTestRule(ConnectActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("iutinfo.lp.orion.kotlin", appContext.packageName)
    }

    @Test
    fun testButtonIsClickable() {
        // Trouver le bouton
        onView(withId(R.id.tvButtonReportPb))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .perform(click())
    }

    @Test
    fun testButtonBackFromConnect() {
        activityRule2.launchActivity(Intent())
        onView(withId(R.id.tvBackButtonConnect))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .perform(click())
    }
}