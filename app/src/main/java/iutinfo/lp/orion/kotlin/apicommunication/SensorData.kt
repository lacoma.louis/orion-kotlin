package iutinfo.lp.orion.kotlin.apicommunication

import android.util.Log
import java.time.LocalDateTime

data class SensorData(val isLightOn: Int,
                      val lastUpdate: LocalDateTime,
                      val nameRoom: String,
                      val sourceIssues: String,
                      val temperature: Float,
                      val temperatureIssues: String) {
}
