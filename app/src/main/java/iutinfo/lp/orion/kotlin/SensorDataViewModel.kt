package iutinfo.lp.orion.kotlin

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import iutinfo.lp.orion.kotlin.apicommunication.SensorData
import iutinfo.lp.orion.kotlin.apicommunication.OrionAPINetwork
import kotlinx.coroutines.launch

class SensorDataViewModel: ViewModel() {

    val response: MutableLiveData<SensorData> = MutableLiveData()
    var error: Boolean = false
    var cmpDialog = 0

    var sharedPreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    fun getSensorData() {
        viewModelScope.launch {
            try {
                response.value = OrionAPINetwork.apiNetwork.getSensorData()
                error = false
            } catch (e: Exception) {
                e.printStackTrace()
                error = true
            }
        }
    }
}