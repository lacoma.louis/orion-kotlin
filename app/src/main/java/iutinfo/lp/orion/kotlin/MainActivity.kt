package iutinfo.lp.orion.kotlin

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import iutinfo.lp.orion.kotlin.apicommunication.SensorData
import kotlinx.android.synthetic.main.activity_main.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.google.firebase.messaging.FirebaseMessaging



class MainActivity : AppCompatActivity() {

    companion object {
        var TAG = "wantedActivity"
    }

    var mHandler: Handler = Handler(Looper.getMainLooper())
    lateinit var viewModel: SensorDataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(SensorDataViewModel::class.java)

        tvLightData.text = resources.getString(R.string.light_data, "--")
        tvTempData.text = resources.getString(R.string.light_data, "--°")
        tvLastUpdated.text = resources.getString(R.string.light_data, "--/-- --:--")
        tvRoomName.text = resources.getString(R.string.light_data, "-----")

        viewModel.sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this)
        viewModel.editor = viewModel.sharedPreferences!!.edit()
        viewModel.editor?.clear()
        viewModel.editor?.apply()

        FirebaseMessaging.getInstance().subscribeToTopic("orion")
    }

    override fun onStart() {
        super.onStart()
        getDataFromAPI()
        handlerLoop()
    }

    override fun onStop() {
        super.onStop()
        mHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

        val visibility = spinner.isVisible
        outState.putBoolean("loaded", visibility)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        if (!savedInstanceState.getBoolean("loaded")) {
            spinner.visibility = GONE
        }

        if (viewModel.response != null) refreshView(viewModel.response.value)
    }

    fun handlerLoop() {
        mHandler.postDelayed({
            try {
                getDataFromAPI()
            } catch (error: Exception) {
                Log.w("Error", "Impossible de récupérer les données" + viewModel.cmpDialog, error)
            }
            handlerLoop()
        }, resources.getInteger(R.integer.refesh_time).toLong())
    }

    fun getDataFromAPI() {
        viewModel.getSensorData()
        if (viewModel.error) {
            if (viewModel.cmpDialog < 1) {
                showDialog()
            }
            viewModel.cmpDialog++
        } else {
            viewModel.response.observe(this, Observer {
                spinner.visibility = GONE
                refreshView(it)
            })
        }
    }

    @SuppressLint("SuspiciousIndentation")
    fun refreshView(sensorData: SensorData?) {
        if (sensorData != null) {
            if(sensorData.sourceIssues == "NoFreshUpdate") {
                ivLastUpdateWarning.visibility = VISIBLE
                ivLastUpdateWarning.setOnClickListener {
                    Toast.makeText(this, "Data is not up to date", Toast.LENGTH_SHORT).show()
                }
                tvLightData.text = resources.getString(R.string.light_data, "--")
                tvTempData.text = resources.getString(R.string.light_data, "--°")
                tvLastUpdated.text = resources.getString(R.string.light_data, "--/-- --:--")
            } else {
                tvLastUpdated.text = resources.getString(R.string.last_updated_time, sensorData.lastUpdate)
                ivLastUpdateWarning.visibility = INVISIBLE
                when (sensorData.temperature) {
                    is Float -> {
                        tvTempData.text = resources.getString(
                            R.string.light_data,
                            sensorData.temperature.toString() + "°"
                        )
                        when(sensorData.temperatureIssues) {
                            "veryHot" -> {
                                ivTempWarning.visibility = VISIBLE
                                ivTempWarning.setOnClickListener {
                                    Toast.makeText(
                                        this,
                                        "The temperature exceeds the high programmed threshold",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                            "veryCold" -> {
                                ivTempWarning.visibility = VISIBLE
                                ivTempWarning.setOnClickListener {
                                    Toast.makeText(
                                        this,
                                        "The temperature exceeds the low programmed threshold",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                            "normal" -> {
                                ivTempWarning.visibility = INVISIBLE
                            }
                            else -> {

                            }
                        }
                    }
                    else -> {
                        tvTempData.text = resources.getString(R.string.light_data, "--°")
                        ivTempWarning.visibility = VISIBLE
                        ivTempWarning.setOnClickListener {
                            Toast.makeText(
                                this,
                                "Couldn't get data for temperature",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
                when (sensorData.isLightOn) {
                    1 -> {
                        tvLightData.text = resources.getString(R.string.light_data, "ON")
                        ivLightWarning.visibility = INVISIBLE
                    }
                    0 -> {
                        tvLightData.text = resources.getString(R.string.light_data, "OFF")
                        ivLightWarning.visibility = INVISIBLE
                    }
                    -1 -> {
                        tvLightData.text = resources.getString(R.string.light_data, "--")
                        ivLightWarning.visibility = VISIBLE
                        ivLightWarning.setOnClickListener {
                            Toast.makeText(
                                this,
                                "The light has no value between 6AM-9PM",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    else -> {
                        tvLightData.text = resources.getString(R.string.light_data, "--")
                        ivLightWarning.visibility = VISIBLE
                        ivLightWarning.setOnClickListener {
                            Toast.makeText(
                                this,
                                "Couldn't get data for light",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
            if(sensorData.nameRoom != "") {
                tvRoomName.text = resources.getString(R.string.room_name, sensorData.nameRoom)
                tvButtonReportPb.setOnClickListener {
                    var intentrp: Intent? = null
                    if(viewModel.sharedPreferences?.getString("userId", "") == "") {
                        intentrp = Intent(this, ConnectActivity::class.java)
                        intentrp.putExtra(TAG, "reportProblem")
                        TAG = "reportProblem"
                    } else {
                        intentrp = Intent(this, ProblemActivity::class.java)
                    }
                    intentrp.putExtra("roomName", tvRoomName.text.toString())
                    startActivity(intentrp)
                }
                /*tvButtonViewProblem.setOnClickListener {
                    var intentvp: Intent? = null
                    if(viewModel.sharedPreferences?.getString("userId", "") == "") {
                        intentvp = Intent(this, ConnectActivity::class.java)
                        intentvp.putExtra(TAG, "viewProblem")
                        TAG = "viewProblem"
                    } else {
                        intentvp = Intent(this, ViewProblemActivity::class.java)
                    }
                    intentvp.putExtra("roomName", tvRoomName.text.toString())
                    startActivity(intentvp)
                }*/

            } else {
                tvRoomName.text = resources.getString(R.string.room_name, sensorData.nameRoom)
            }
        } else {

        }
    }

    fun parseDate(date: LocalDateTime): String {
        return date.format(DateTimeFormatter.ofPattern("dd/MM HH:mm"))
    }

    fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error detected")
        if (!isNetworkAvailable(this)) {
            builder.setMessage("Please connect to a network")
            builder.setPositiveButton(R.string.yes) { dialog, which ->
            }
        } else builder.setMessage("The data couldn't be retrieved")
        builder.show()
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}