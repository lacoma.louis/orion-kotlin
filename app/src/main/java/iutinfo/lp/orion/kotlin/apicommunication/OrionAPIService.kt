package iutinfo.lp.orion.kotlin.apicommunication

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface OrionAPIService {
    @GET("room-information")
    suspend fun getSensorData(): SensorData

    @Multipart
    @POST("room-issue")
    fun createProblem(@Part("cardId") userId: RequestBody,
                      @Part("description") desc: RequestBody,
                      @Part("nameRoom") nameRoom: RequestBody,
                      @Part("title") title: RequestBody,
                      @Part pictureFile: MultipartBody.Part): Call<Unit?>?

    @GET("room-issue")
    fun getProblemData(): ProblemData
}