package iutinfo.lp.orion.kotlin

import android.content.SharedPreferences
import android.database.Observable
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import iutinfo.lp.orion.kotlin.apicommunication.OrionAPINetwork
import iutinfo.lp.orion.kotlin.apicommunication.ProblemData
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.InputStream


class ProblemActivityViewModel: ViewModel() {

    var userId: String? = null
    var roomName: String? = null
    var titletext: String? = null
    var desctext: String? = null
    var uri: Uri? = null
    var bitmap: Bitmap? = null
    var photoFile: File? = null

    lateinit var report: ProblemData
    lateinit var sharedPreferences: SharedPreferences
    var call: Call<Unit?>? = null
    var response: Observable<ResponseBody>? = null

    fun postProblem(): Call<Unit?>? {
        if(userId == null || roomName == null || titletext == null || desctext == null || uri == null) return call
        report = ProblemData(userId!!, roomName!!, null, titletext!!, desctext!!, bitmap!!)
        viewModelScope.launch {
            try {
                Log.i("post", "uri is " + uri)
                Log.i("post", "is file file ? " + photoFile!!.isFile)
                val fbody: RequestBody = RequestBody.create(
                    MediaType.parse("image/*"),
                    photoFile
                )

                Log.i("post", "what happen 1")

                call = OrionAPINetwork.apiNetwork.createProblem(
                    RequestBody.create(MediaType.parse("text/plain"), report.userId),
                    RequestBody.create(MediaType.parse("text/plain"), report.pbDesc),
                    RequestBody.create(MediaType.parse("text/plain"), report.roomName),
                    RequestBody.create(MediaType.parse("text/plain"), report.pbTitle),
                    MultipartBody.Part.createFormData("pictureFile", photoFile!!.name, fbody))

                Log.i("post", "what happen 2")

                call!!.enqueue(object : Callback<Unit?> {
                    override fun onFailure(call: Call<Unit?>, t: Throwable) {
                        Log.i("post", "response code is big fail : " + call)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<Unit?>,
                        response: Response<Unit?>
                    ) {
                        Log.i("post", "response code : " + response.code())
                        Log.i("post", "response body : " + response.body())
                        Log.i("post", "response header : " + response.headers())
                        Log.i("post", "response message : " + response.message())
                        Log.i("post","response error body : " + response.errorBody()?.string())
                    }
                })
                Log.i("post", "what happen 3")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        Log.i("post", "what happen 4")
        return call
    }
}