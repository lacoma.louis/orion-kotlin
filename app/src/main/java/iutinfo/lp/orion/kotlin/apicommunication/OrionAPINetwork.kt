package iutinfo.lp.orion.kotlin.apicommunication

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime

object OrionAPINetwork {

    val apiNetwork by lazy<OrionAPIService> {
        val gson = GsonBuilder().registerTypeAdapter(LocalDateTime::class.java,  JsonDeserializer<LocalDateTime>() {
            json, typeOf, context ->
            LocalDateTime.parse(json.asString).plusHours(1)
        }).create()

        Retrofit.Builder()
                .baseUrl("https://api-orion.lp-cloud.tech/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(OrionAPIService::class.java)
    }
}