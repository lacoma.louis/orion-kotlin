package iutinfo.lp.orion.kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_single_problem.*
import kotlinx.android.synthetic.main.connect_activity.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SingleProblemActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_problem)

        tvProblemTitle.text = resources.getString(R.string.problem_title, "problem title")
        tvProblemDesc.text = resources.getString(R.string.problem_desc, "problem description")
        tvProblemAuthDate.text = resources.getString(R.string.problem_auth_date, "Louis", "8 march 2023 11h33")

        val intent: Intent = getIntent()

        tvProblemTitle.text = resources.getString(
            R.string.problem_title,
            intent.getStringExtra("title"))

        tvProblemDesc.text = resources.getString(
            R.string.problem_title,
            intent.getStringExtra("description"))
        
        tvProblemAuthDate.text = resources.getString(
            R.string.problem_auth_date,
            intent.getStringExtra("userName"),
            parseDate(LocalDateTime.now()))
            //intent.getStringExtra("date"))

        tvBackButtonReport.setOnClickListener {
            finish()
        }
    }

    fun parseDate(date: LocalDateTime): String {
        return date.format(DateTimeFormatter.ofPattern("dd MMM HH:mm"))
    }
}