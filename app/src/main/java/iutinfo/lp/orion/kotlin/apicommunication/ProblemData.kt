package iutinfo.lp.orion.kotlin.apicommunication

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.util.UUID

data class ProblemData (
    @SerializedName("userid") val userId: String,
    @SerializedName("roomName") val roomName: String,
    @SerializedName("date") val dateTime: LocalDateTime? = LocalDateTime.now(),
    @SerializedName("title") val pbTitle: String,
    @SerializedName("description") val pbDesc: String,
    val pbPicture: Bitmap? = null)