package iutinfo.lp.orion.kotlin

import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.MifareClassic
import android.nfc.tech.MifareUltralight
import android.os.*
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.connect_activity.*
import kotlin.experimental.and

class ConnectActivity : AppCompatActivity() {
    //Intialize attributes
    var nfcAdapter: NfcAdapter? = null
    var pendingIntent: PendingIntent? = null
    var roomName: String? = null

    var mHandler: Handler = Handler(Looper.getMainLooper())
    var sharedPreferences: SharedPreferences? = null
    var editor: Editor? = null
    var wantedActivity: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.connect_activity)

        val intent = getIntent()
        roomName = intent.getStringExtra("roomName")
        wantedActivity = intent.getStringExtra("wantedActivity")

        //Initialise NfcAdapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        loadView()
        tvNFCActivate.setOnClickListener {
            nfcActivate()
        }
        tvBackButtonConnect.setOnClickListener {
            finish()
        }
        //If no NfcAdapter, display that the device has no NFC

        pendingIntent = PendingIntent.getActivity(
            this,
            0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
            PendingIntent.FLAG_MUTABLE
        )
    }

    companion object {
        const val TAG = "nfc_test"
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter?.enableForegroundDispatch(this, pendingIntent, null, null)
    }

    override fun onRestart() {
        super.onRestart()
        nfcActivate()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacksAndMessages(null)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        resolveIntent(intent)
    }

    private fun resolveIntent(intent: Intent) {
        val action = intent.action
        if (NfcAdapter.ACTION_TAG_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
            val tag: Tag = (intent.getParcelableExtra<Parcelable>(NfcAdapter.EXTRA_TAG) as Tag?)!!

            if (tag.id != null) {
                viewSelector(tag)
            }
            val payload: ByteArray = detectTagData(tag)!!.toByteArray()
        }
    }

    fun viewSelector(tag: Tag) {
        if (wantedActivity == "viewProblem") {
            val intent: Intent = Intent(this, ViewProblemActivity::class.java)
            editor!!.putString("userId", toHex(tag.id))
            editor!!.apply()
            intent.putExtra("roomName", roomName)
            startActivity(intent)
            finish()
        } else if (wantedActivity == "reportProblem") {
            val intent: Intent = Intent(this, ProblemActivity::class.java)
            editor!!.putString("userId", toHex(tag.id))
            editor!!.apply()
            intent.putExtra("roomName", roomName)
            startActivity(intent)
            finish()
        }
    }

    private fun detectTagData(tag: Tag): String? {
        val sb = StringBuilder()
        val id = tag.id
        sb.append("ID (hex): ").append(toHex(id)).append('\n')
        sb.append("ID (dec): ").append(toDec(id)).append('\n')
        val prefix = "android.nfc.tech."
        sb.append("Technologies: ")
        for (tech in tag.techList) {
            sb.append(tech.substring(prefix.length))
            sb.append(", ")
        }
        sb.delete(sb.length - 2, sb.length)
        for (tech in tag.techList) {
            if (tech == MifareClassic::class.java.name) {
                sb.append('\n')
                var type = "Unknown"
                try {
                    val mifareTag = MifareClassic.get(tag)
                    when (mifareTag.type) {
                        MifareClassic.TYPE_CLASSIC -> type = "Classic"
                        MifareClassic.TYPE_PLUS -> type = "Plus"
                        MifareClassic.TYPE_PRO -> type = "Pro"
                    }
                    sb.append("Mifare Classic type: ")
                    sb.append(type)
                    sb.append('\n')
                    sb.append("Mifare size: ")
                    sb.append(mifareTag.size.toString() + " bytes")
                    sb.append('\n')
                    sb.append("Mifare sectors: ")
                    sb.append(mifareTag.sectorCount)
                    sb.append('\n')
                    sb.append("Mifare blocks: ")
                    sb.append(mifareTag.blockCount)
                } catch (e: Exception) {
                    sb.append("Mifare classic error: " + e.message)
                }
            }
            if (tech == MifareUltralight::class.java.name) {
                sb.append('\n')
                val mifareUlTag = MifareUltralight.get(tag)
                var type = "Unknown"
                when (mifareUlTag.type) {
                    MifareUltralight.TYPE_ULTRALIGHT -> type = "Ultralight"
                    MifareUltralight.TYPE_ULTRALIGHT_C -> type = "Ultralight C"
                }
                sb.append("Mifare Ultralight type: ")
                sb.append(type)
            }
        }
        Log.v("test", sb.toString())
        return sb.toString()
    }

    private fun toHex(bytes: ByteArray): String {
        val sb = StringBuilder()
        for (i in bytes.indices) {
            val b: Byte = bytes[i] and 0xff.toByte()
            if (i > 0) {
                sb.append(":")
            }
            var hex = Integer.toHexString(b.toInt()).toUpperCase()
            if (hex.length < 2) {
                hex = "0$hex"
            }
            if (hex.startsWith("F")) {
                hex = hex.substring(hex.length-2, hex.length)
            }
            sb.append(hex)
        }
        print(sb.toString())
        return sb.toString()
    }


    private fun toDec(bytes: ByteArray): Long {
        var result: Long = 0
        var factor: Long = 1
        for (i in bytes.indices) {
            val value: Long = (bytes[i] and 0xffL.toByte()).toLong()
            result += value * factor
            factor *= 256L
        }
        return result
    }

    fun nfcActivate() {
        mHandler.postDelayed({
            if(!NfcAdapter.getDefaultAdapter(this).isEnabled) {
                checkParameterNFC()
            } else {
                nfcAdapter = NfcAdapter.getDefaultAdapter(this)
            }
            loadView()
            nfcActivate()
        }, 300)
    }

    fun loadView() {
        if (nfcAdapter == null) {
            //relancer l'activité
            val builder = AlertDialog.Builder(this)
            builder.setTitle("NO NFC Capabilities")
            builder.setMessage("your phone does not have NFC capabilities, please try again with another phone")
            builder.show()
            tvButtonReportPb.visibility = View.INVISIBLE
            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            //si le nfc est activé
            if (NfcAdapter.getDefaultAdapter(this).isEnabled) {
                tvNFCCard.visibility = View.VISIBLE
                tvConnectHintText2.visibility = View.VISIBLE
                tvNFCActivate.visibility = View.INVISIBLE
                tvConnectHintText.visibility = View.INVISIBLE
                sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(this)
                editor = sharedPreferences!!.edit()
            }
            //si le nfc n'est pas activé
            else {
                tvNFCCard.visibility = View.INVISIBLE
                tvConnectHintText2.visibility = View.INVISIBLE
                tvNFCActivate.visibility = View.VISIBLE
                tvConnectHintText.visibility = View.VISIBLE
//                tvButtonConnectNfc.visibility = View.VISIBLE
            }
        }
    }

    fun checkParameterNFC() {
        var intent: Intent?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            intent = Intent(android.provider.Settings.ACTION_NFC_SETTINGS)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            startActivity(intent)
            return
        } else {
            intent = Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            startActivity(intent)
            return
        }
    }
}