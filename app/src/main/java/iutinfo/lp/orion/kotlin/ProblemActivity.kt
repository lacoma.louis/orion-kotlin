package iutinfo.lp.orion.kotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.report_problem.*
import kotlinx.android.synthetic.main.report_problem.view.*
import java.io.File


class ProblemActivity : AppCompatActivity() {

    val takePicture: ActivityResultLauncher<Uri> = registerForActivityResult(ActivityResultContracts.TakePicture()) { boolean: Boolean? ->
        Log.i("Picture", "here is uri" + viewModel.uri)
        if(boolean!!) {
            val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), viewModel.uri)
            viewModel.bitmap = bitmap
            ivPicturePreview.setImageBitmap(bitmap)
        }
    }

    lateinit var viewModel: ProblemActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.report_problem)

        viewModel = ViewModelProvider(this).get(ProblemActivityViewModel::class.java)
        viewModel.sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this)

        val intent = getIntent()
        viewModel.userId = viewModel.sharedPreferences?.getString("userId", null)
        viewModel.roomName = intent.getStringExtra("roomName")
        tvRoomNameReport.text = viewModel.roomName

        tvButtonPhoto.setOnClickListener {
            viewModel.photoFile = File.createTempFile(
                "IMG_",
                ".jpg",
                getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )

            viewModel.uri = FileProvider.getUriForFile(
                this,
                "${packageName}.provider",
                viewModel.photoFile as File
            )
            takePicture.launch(viewModel.uri)
        }

        ivBackButtonReport.setOnClickListener {
            finish()
        }

        tvButtonSend.setOnClickListener {
            if(etPbTitle.text.toString() != "" && etPbDesc.text.toString() != "" && viewModel.uri != null){
                viewModel.titletext = etPbTitle.text.toString()
                viewModel.desctext = etPbDesc.text.toString()
                sendProblemToAPI()
                Toast.makeText(this, "Your problem was sent", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun sendProblemToAPI() {
        val response = viewModel.postProblem()
        Log.i("post", "" + response.toString())
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        /*etPbTitle.text = viewModel.titletext as Editable
        etPbDesc.text = viewModel.desctext as Editable*/
        if(viewModel.uri != null) {
            val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), viewModel.uri)
            ivPicturePreview.setImageBitmap(bitmap)
        }
    }

    companion object {
        val PHOTO_RESULT = "Photo"
    }
}