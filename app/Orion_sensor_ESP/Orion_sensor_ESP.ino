#include "DHT.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#define DHTPIN 8    
#define DHTTYPE DHT22  
DHT dht(DHTPIN, DHTTYPE);
float sensorValue;
String json; 

const char* ssid = "lpdevmob";
const char* password = "projetiot";

const char* apiName = "https://api-orion.lp-cloud.tech/room-information";


void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!")); 
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println(".");
  }
  Serial.println(WiFi.localIP());
  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(10000);

  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  if (isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
 
  sensorValue = analogRead(17);

  DynamicJsonDocument doc(1024);
  doc["nameRoom"] = "Niort Salle Tech";
  doc["temperature"] = t;
  doc["lightPower"] = sensorValue;

  String json;  
  serializeJsonPretty(doc, json);
  Serial.println(json);

  if(WiFi.status() == WL_CONNECTED) {

    HTTPClient http;
    http.begin(apiName);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST(json);
    Serial.println(httpResponseCode);
    http.end();

  } else {
    Serial.println("WiFi Disconnected");
  }
}